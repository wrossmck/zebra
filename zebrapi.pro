%% Adapted by Ross McKinley (mckinlrw@tcd.ie)
%% To the Life Magazine Rule-set
%% Trinity College Dublin, Ireland

%  Original zebra puzzle solution by
%  Jonathan Mohr (mohrj@augustana.ab.ca)
%  Augustana University College, Camrose, AB, Canada  T4V 2R3
%  Available at: http://www.augustana.ca/~mohrj/courses/2000.fall/csc370/lecture_notes/prolog_examples/zebra_puzzle.pro

solve(Who, S) :-
% There are five houses.
% The order of the sublists is the order of the houses, left to right.
    S = [[C1,N1,P1,D1,S1],
         [C2,N2,P2,D2,S2],
         [C3,N3,P3,D3,S3],
         [C4,N4,P4,D4,S4],
         [C5,N5,P5,D5,S5]],
%% 1 There are five houses

%2 The Englishman lives in the red house.
    member([red, englishman, _, _, _], S),
% The Spaniard has a dog.
    member([_, spaniard, dog, _, _], S),
% In the green house, they drink coffee.
    member([green, _, _, coffee, _], S),
% The ukrainian drinks tea.
    member([_, ukrainian, _, tea, _], S),
% The green house is immediately to the right of the ivory house.
    left_of([ivory |_], [green |_], S),
% 7 The Old Gold smoker owns snails.
    member([_, _, snails, _, oldgold], S),
% 8 In the yellow house, they smoke Dunhill.
    member([yellow, _, _, _, kools], S),
% 9 In the middle house, they drink milk.
    D3 = milk,
% 10 The Norwegian lives in the first house.
    N1 = norwegian,
% 11 The man who smokes Chesterfields lives in the house next to the man with the fox.
    next_to([_, _, _, _, chesterfields], [_, _, fox |_], S),
% 12 Kools are smoked in the house next to the house where the horse is kept.
    next_to([_, _, _, _, kools], [_, _, horse |_], S),
% 13 The Lucky Strike smoker drinks orange juice.
    member([_, _, _, orangejuice, luckystrike], S),
% 14 The Japanese smokes Parliaments.
    member([_, japanese, _, _, parliaments], S),
% The Norwegian lives next to the blue house.
    next_to([_, norwegian |_], [blue |_], S),

%
% The puzzle is so constrained that the following checks are not really
% required, but I include them for completeness (since one would not
% know in advance of solving the puzzle if it were fully constrained
% or not).
%
% Each house has its own unique color.
    C1 \== C2, C1 \== C3, C1 \== C4, C1 \== C5,
    C2 \== C3, C2 \== C4, C2 \== C5,
    C3 \== C4, C3 \== C5, C4 \== C5,
% All house owners are of different nationalities.
    N1 \== N2, N1 \== N3, N1 \== N4, N1 \== N5,
    N2 \== N3, N2 \== N4, N2 \== N5,
    N3 \== N4, N3 \== N5, N4 \== N5,
% They all have different pets.
    P1 \== P2, P1 \== P3, P1 \== P4, P1 \== P5,
    P2 \== P3, P2 \== P4, P2 \== P5,
    P3 \== P4, P3 \== P5, P4 \== P5,
% They all drink different drinks.
    D1 \== D2, D1 \== D3, D1 \== D4, D1 \== D5,
    D2 \== D3, D2 \== D4, D2 \== D5,
    D3 \== D4, D3 \== D5, D4 \== D5,
% They all smoke different cigarettes.
    S1 \== S2, S1 \== S3, S1 \== S4, S1 \== S5,
    S2 \== S3, S2 \== S4, S2 \== S5,
    S3 \== S4, S3 \== S5, S4 \== S5,

% Who owns the zebra?
    member([_, Who, zebra, _, _], S).

% Or, replace the last line by:
%   format("The ~w owns the zebra.", Who).

left_of(L1, L2, [L1, L2 |_]).
left_of(L1, L2, [_| Rest ]) :-
    left_of(L1, L2, Rest).
    
next_to(L1, L2, S) :-
    left_of(L1, L2, S).
next_to(L1, L2, S) :-
    left_of(L2, L1, S).

:- time(( solve(Who, S), maplist(writeln,S), nl, write(Who), nl, nl, fail 
             ; write('No more solutions.') )).
%% this times how long it takes to get ALL possible solutions. 
%% even though the Zebra Puzzle has a unique solution, that can't 
%% be known before computation.