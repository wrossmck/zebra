%% Adapted by Ross McKinley (mckinlrw@tcd.ie)
%% Trinity College Dublin, Ireland

%% Original Mini-Zebra Puzzle by 
%% Henry Kautz (kautz@cs.rochester.edu)
%% University of Rochester, Rochester, NY 14627. 
%% Available: http://courses.cs.washington.edu/courses/cse573/05au/assign3.htm [Accessed 2014/02/11 2014].

%  Original full zebra puzzle solution by
%  Jonathan Mohr (mohrj@augustana.ab.ca)
%  Augustana University College, Camrose, AB, Canada  T4V 2R3
%  Available at: http://www.augustana.ca/~mohrj/courses/2000.fall/csc370/lecture_notes/prolog_examples/zebra_not.pro


solve(Who, [N1, D1, P1]) :-
% There are five houses.
% All house owners are of different nationalities.
	N = [spanish, ukrainian, norwegian],
	permutation(N,N1),
	%write(N1),nl,
% They all have different pets.
	P = [fox, dog, zebra],
	permutation(P,P1),
	%write(P1),nl,
% They all drink different drinks.
	D = [milk, tea, juice],
	permutation(D,D1),
	%write(D1), nl,
% The Spaniard owns a dog.
	samepos(spanish, N1, dog, P1),
% The ukrainian drinks tea.
	samepos(ukrainian, N1, tea, D1),
% The man in the third house drinks milk.
	D1 = [_, _, milk],
% The Norwegian lives next to the tea drinker.
	next_to(norwegian, N1, tea, D1),
% The juice drinker owns a fox.
	samepos(juice, D1, fox, P1),
% The fox is next door to the dog.
	next_to(fox, P1, dog, P1),
% Who owns the zebra?
	samepos(zebra, P1, Who, N1).

permutation( L, [ H | T ] ) :-
	append( U, [ H | V ], L ),
	append( U, V, W ),
	permutation( W, T ).
permutation( [], [] ).

samepos(A, [A|_], B, [B|_]).
samepos(A, [_|T1], B, [_|T2]) :-
	samepos(A, T1, B, T2).

inorder(L1, L2, [L1, L2 |_]).
inorder(L1, L2, [_| Rest ]) :-
    inorder(L1, L2, Rest).
    
next_to(A, [A|_], B, [_,B|_]).
next_to(A, [_,A|_], B, [B|_]).
next_to(A, [_|T1], B, [_|T2]) :-
    next_to(A, T1, B, T2).

:- time(( solve(Who, S), maplist(writeln,S), nl, write(Who), nl, nl, fail 
             ; write('No more solutions.') )).
%% this times how long it takes to get ALL possible solutions. 
%% even though the Zebra Puzzle has a unique solution, that can't 
%% be known before computation begins.