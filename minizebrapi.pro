%% Adapted by Ross McKinley (mckinlrw@tcd.ie)
%% Trinity College Dublin, Ireland

%% Original Mini-Zebra Puzzle by 
%% Henry Kautz (kautz@cs.rochester.edu)
%% University of Rochester, Rochester, NY 14627. 
%% Available: http://courses.cs.washington.edu/courses/cse573/05au/assign3.htm [Accessed 2014/02/11 2014].

%  Original full zebra puzzle solution by
%  Jonathan Mohr (mohrj@augustana.ab.ca)
%  Augustana University College, Camrose, AB, Canada  T4V 2R3
%  Available at: http://www.augustana.ca/~mohrj/courses/2000.fall/csc370/lecture_notes/prolog_examples/zebra_puzzle.pro


solve(Who, S):-
%% There are three houses in a row on street. Each house is inhabited by a man of a different nationality, who has a different pet, and drinks a different beverage.

%% 1. The Spaniard owns a dog.
%% 2. The ukrainian drinks tea.
%% 3. The man in the third house drinks milk.
%% 4. The Norwegian lives next to the tea drinker.
%% 5. The juice drinker owns a fox.
%% 6. The fox is next door to the dog.

%% Question: Who owns the zebra? More generally, for each house: who lives there, what pet does he have, and what does he drink?
%% define House       [Nationality Drink Pet];

% (The constraints that all colors, etc., are different can only be
% applied after all or most of the variables have been instantiated.
% See below.)

% S = [[Nationality1, Drink1, Pet1] |_]
% The order of the sublists is the order of the houses, left to right.
    S = [[N1,D1,P1],
         [N2,D2,P2],
         [N3,D3,P3]],

% The Spaniard own a dog.
    member(['spaniard', _, dog], S),
% The ukrainian drinks tea.
    member(['ukrainian', tea, _], S),
% The man in the third house drinks milk.
    D3 = milk,
% The Norwegian lives next to the tea drinker.
    next_to(['norwegian', _, _], [_, tea, _ |_], S),
% The juice drinker owns a fox.
    member([_, juice, fox], S),
% The fox is next door to the dog.
    next_to([_, _, fox], [_, _, dog |_], S),

%
% The puzzle is so constrained that the following checks are not really
% required, but I include them for completeness (since one would not
% know in advance of solving the puzzle if it were fully constrained
% or not).
%
% All house owners are of different nationalities.
    N1 \== N2, N1 \== N3, N2 \== N3,
% They all have different pets.
    P1 \== P2, P1 \== P3, P2 \== P3, 
% They all drink different drinks.
    D1 \== D2, D1 \== D3, D2 \== D3,

% Who owns the zebra?
    member([Who, _, zebra], S).


left_of(L1, L2, [L1, L2 |_]).
left_of(L1, L2, [_| Rest ]) :-
    left_of(L1, L2, Rest).
    
next_to(L1, L2, S) :-
    left_of(L1, L2, S).
next_to(L1, L2, S) :-
    left_of(L2, L1, S).

:- time(( solve(Who, S), maplist(writeln,S), nl, write(Who), nl, nl, fail 
             ; write('No more solutions.') )).
%% this times how long it takes to get ALL possible solutions. 
%% even though the Zebra Puzzle has a unique solution, that can't 
%% be known before computation begins.