%% Adapted by Ross McKinley (mckinlrw@tcd.ie)
%% To the Life Magazine Rule-set
%% Trinity College Dublin, Ireland

%  Original zebra puzzle solution by
%  Jonathan Mohr (mohrj@augustana.ab.ca)
%  Augustana University College, Camrose, AB, Canada  T4V 2R3
%  Available at: http://www.augustana.ca/~mohrj/courses/2000.fall/csc370/lecture_notes/prolog_examples/zebra_not.pro


solve(Who, [C1, N1, P1, D1, S1]) :-
% 1 There are five houses.
% X Each house has its own unique color.
	C = [red, green, ivory, yellow, blue],
	permutation(C,C1),
% X All house owners are of different nationalities.
	N = [englishman, spaniard, ukrainian, norwegian, japanese],
	permutation(N,N1),
% X They all have different pets.
	P = [dogs, snails, fox, horse, zebra],
	permutation(P,P1),
% X They all drink different drinks.
	D = [orangejuice, coffee, milk, tea, water],
	permutation(D,D1),
% X They all smoke different cigarettes.
	S = [oldgold, kools, chesterfields, luckystrike, parliaments],
	permutation(S,S1),
% 2 The English man lives in the red house.
	samepos(red, C1, english, N1),
% 3 The spaniard has a dog.
	samepos(spaniard, N1, dogs, P1),
% 4 In the green house, they drink coffee.
	samepos(green, C1, coffee, D1),
% 5 The ukrainian drinks tea.
	samepos(ukrainian, N1, tea, D1),
% 6 The green house is on the right side of the ivory house.
	inorder(ivory, green, C1),
% 7 The man who smokes oldgold has snails.
	samepos(snails, P1, oldgold, S1),
% 8 In the yellow house, they smoke kools.
	samepos(yellow, C1, kools, S1),
% 9 In the middle house, they drink milk.
	D1 = [_, _, milk, _, _],
% 10 The Norwegian lives in the first house.
	N1 = [norwegian | _],
% 11 The man who smokes Chesterfields lives in the house next to the man with the fox.
	next_to(chesterfields, S1, fox, P1),
% In the house next to the house with the horse, they smoke kools.
	next_to(dunhill, S1, horse, P1),
% The man who smokes luckystrike drinks orangejuice.
	samepos(orangejuice, D1, luckystrike, S1),
% The japanese smokes parliaments.
	samepos(japanese, N1, parliaments, S1),
% The Norwegian lives next to the blue house.
	next_to(norwegian, N1, blue, C1),
% Who owns the zebra?
	samepos(zebra, P1, Who, N1).

permutation( L, [ H | T ] ) :-
	append( U, [ H | V ], L ),
	append( U, V, W ),
	permutation( W, T ).
permutation( [], [] ).

samepos(A, [A|_], B, [B|_]).
samepos(A, [_|T1], B, [_|T2]) :-
	samepos(A, T1, B, T2).

inorder(L1, L2, [L1, L2 |_]).
inorder(L1, L2, [_| Rest ]) :-
    inorder(L1, L2, Rest).
    
next_to(A, [A|_], B, [_,B|_]).
next_to(A, [_,A|_], B, [B|_]).
next_to(A, [_|T1], B, [_|T2]) :-
    next_to(A, T1, B, T2).

:- time(( solve(Who, S), maplist(writeln,S), nl, write(Who), nl, nl, fail 
             ; write('No more solutions.') )).
%% this times how long it takes to get ALL possible solutions. 
%% even though the Zebra Puzzle has a unique solution, that can't 
%% be known before computation.