#Investigating The Zebra Puzzle and Finite State Methods
[Ross McKinley](http://wrossmck.bitbucket.org)

## Abstract

This report looks at how Finite State Methods solve Constraint Satisfaction Problems, using the Xerox Finite State Toolkit and a comparable open source finite state toolkit developed by Mans Hulden, foma. The report then compares the performance with traditional Constraint Satisfaction Problem Methods, such as a primitive Backtracking method, and a more advanced Partial Instantiation method.
Specifically, this report looks at if Finite State Methods are a viable alternative for solving Constraint Satisfaction Problems and if not, why not. This is done by using the Zebra Puzzle, and a smaller version, the Mini-Zebra Puzzle, as example puzzles.
This report found that Traditional methods will perform well in a solution-rich environment, whereas Finite State Methods will perform well regardless of number of solutions. This tradeoff is best made on a per-problem basis, on the assumption that intrinsic knowledge is gained a priori about a rough number of solutions that exist to the problem. In the case that no prior knowledge exists, it then depends on the number of solutions required. Finite State Methods perform best when all solutions are desired, and Traditional Methods perform well when merely the first solution is required.
This report also found that Finite State Methods could be greatly optimised by developing entailments to see which constraints are necessary to gain a solution, before commencement of evaluation.
## Online Resources
[The full Report is available here](https://bitbucket.org/wrossmck/zebra/raw/master/InvestigatingTheZebraPuzzleandFiniteStateMethods.pdf)
The Zebra Puzzle code is available here:
* [foma/xfst](https://bitbucket.org/wrossmck/zebra/raw/master/zebra.foma)* [backtracking in prolog](https://bitbucket.org/wrossmck/zebra/raw/master/zebrabt.pro)* [partial Instantiation in prolog](https://bitbucket.org/wrossmck/zebra/raw/master/zebrapi.pro)
The Mini-Zebra Puzzle code is available here:
* [foma/xfst](https://bitbucket.org/wrossmck/zebra/raw/master/minizebra.foma) 
* [backtracking in prolog](https://bitbucket.org/wrossmck/zebra/raw/master/minizebrabt.pro) 
* [partial Instantiation in prolog](https://bitbucket.org/wrossmck/zebra/raw/master/minizebrapi.pro)
